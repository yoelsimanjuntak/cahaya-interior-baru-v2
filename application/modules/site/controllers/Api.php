<?php
class Api extends MY_Controller {
  public function __construct()
  {
      parent::__construct();
  }

  public function getdata() {
    $dateFrom = $this->input->get("date_from");
    $dateTo = $this->input->get("date_to");
    $dateFrFmt = date('Y-m-d 00:00:00');
    $dateToFmt = date('Y-m-d 23:59:59');
    if(!empty($dateFrom)) $dateFrFmt = date('Y-m-d H:i:s', strtotime(str_replace(' ','+',$dateFrom)));
    if(!empty($dateTo)) $dateToFmt = date('Y-m-d H:i:s', strtotime(str_replace(' ','+',$dateTo)));
    $cond = " dat.Create_Date >= '$dateFrFmt' and dat.Create_Date <= '$dateToFmt' ";
    $q = @"
    select
    sum(tbl.Jlh_ODP + tbl.Jlh_ODPS) as total_odp,
    sum(tbl.Jlh_PDP) as total_pdp,
    sum(tbl.Jlh_Positif) as total_terkonfirmasi,
    0 as total_sembuh,
    0 as total_meninggal,
    tbl.Create_Date as update_datetime
    from (
          select
          kec.Kd_Kecamatan,
          kec.Nm_Kecamatan,
          ifnull((select dat.Jlh_OTGP from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan and $cond order by dat.Create_Date desc limit 1), 0) as Jlh_OTGP,
          ifnull((select dat.Jlh_OTGS from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan and $cond order by dat.Create_Date desc limit 1), 0) as Jlh_OTGS,
          ifnull((select dat.Jlh_ODP from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan and $cond order by dat.Create_Date desc limit 1), 0) as Jlh_ODP,
          ifnull((select dat.Jlh_ODPS from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan and $cond order by dat.Create_Date desc limit 1), 0) as Jlh_ODPS,
          ifnull((select dat.Jlh_PDP from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan and $cond order by dat.Create_Date desc limit 1), 0) as Jlh_PDP,
          ifnull((select dat.Jlh_Positif from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan and $cond order by dat.Create_Date desc limit 1), 0) as Jlh_Positif,
          (select dat.Create_Date from covid19_kecamatan_data dat where $cond order by dat.Create_Date desc limit 1) as Create_Date,
          (select u.Name from covid19_kecamatan_data dat left join _userinformation u on. u.UserName = dat.Create_By order by dat.Create_Date desc limit 1) as Name
          from covid19_kecamatan kec
    ) tbl
    ";
    $res = $this->db->query($q)->row_array();
    echo json_encode(array(
      "Response_code" => 'RC200',
      "Kode_wilayah" => '12.16',
      "Message" => 'Berhasil',
      "Description" => 'Berhasil',
      "Date_from" => $dateFrFmt,
      "Date_to" => $dateToFmt,
      "Data" => array_merge($res, array('detil_odp'=>array(), 'detil_pdp'=>array(), 'detil_terkonfirmasi'=>array(), 'detil_sembuh'=>array(), 'detil_meninggal'=>array()))
    ));
  }

  public function login() {
    $this->load->model('muser');
    $json = file_get_contents('php://input');
    $data = json_decode($json);

    if(empty($json)) {
      ShowJsonError('JSON tidak valid.');
      return;
    }

    $username = $data->UserName;
    $password = $data->Password;

    if($this->muser->authenticate($username, $password)) {
      if($this->muser->IsSuspend($username)) {
        ShowJsonError('Akun anda di suspend.');
        return;
      }

      $this->db->where(COL_USERNAME, $username);
      $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));

      $userdetails = $this->muser->getdetails($username);
      $token = GetEncryption($userdetails[COL_USERNAME]);
      ShowJsonSuccess('Login berhasil.', array(
        'UserName' => $userdetails[COL_USERNAME],
        'Email' => $userdetails[COL_EMAIL],
        'Role' => $userdetails[COL_ROLENAME],
        'ID_Role' => $userdetails[COL_ROLEID],
        'Nama' => $userdetails[COL_NAME],
        'Jabatan' => $userdetails[COL_JABATAN],
        'No_Identitas' => $userdetails[COL_NO_IDENTITAS],
        'No_HP' => $userdetails[COL_NO_HANDPHONE],
        'Token' => $token
      ));
    } else {
      ShowJsonError('Username / password tidak tepat.');
    }
  }

  public function fetch_data() {
    $this->load->model('muser');
    $json = file_get_contents('php://input');
    if(empty($json)) {
      ShowJsonError('JSON tidak valid.');
      return;
    }

    $data = json_decode($json);
    $token = $data->Token;
    $filter = $data->Filter;

    $ruser = $this->muser->getdetails(GetDecryption($token));
    if(empty($ruser)) {
      ShowJsonError('Token tidak valid.');
      return;
    }

    if(!empty($filter)) {
      foreach ($filter as $f)
      {
        $cond = (array) $f->condition;
        if($f->type == 'like') {
          foreach ($cond as $key => $value) {
            if(!empty($cond[$key])) $this->db->like('tbl_keluar_masuk.'.$key, $value);
          }
        } else {
          foreach ($cond as $key => $value) {
            if(!empty($cond[$key])) $this->db->where('tbl_keluar_masuk.'.$key, $value);
          }
        }
      }
    }

    $res = $this->db->get(TBL_TBL_KELUAR_MASUK)->result_array();
    ShowJsonSuccess('SUCCESS', $res);
  }

  public function insert_data() {
    $this->load->model('muser');
    $json = file_get_contents('php://input');
    if(empty($json)) {
      ShowJsonError('JSON tidak valid.');
      return;
    }

    $data = json_decode($json);
    $token = $data->Token;
    $data = (array) $data->Data;
    $insert = array();

    $ruser = $this->muser->getdetails(GetDecryption($token));
    if(empty($ruser)) {
      ShowJsonError('Token tidak valid.');
      return;
    }

    $this->db->select('*');
    if(!empty($data)) {
      foreach ($data as $key => $value) {
        if(!empty($data[$key]) && !array_key_exists($key, $insert)) $insert[$key] = $value;
      }
    }

    $res = false;
    if(!empty($insert)) {
      $insert[COL_TGL_INSERT] = date('Y-m-d H:i:s');
      $res = $this->db->insert(TBL_TBL_KELUAR_MASUK, $insert);
    }

    if($res) {
      ShowJsonSuccess('SUCCESS');
      return;
    } else {
      ShowJsonError('GAGAL');
      return;
    }
  }

  public function update_data() {
    $this->load->model('muser');
    $json = file_get_contents('php://input');
    if(empty($json)) {
      ShowJsonError('JSON tidak valid.');
      return;
    }

    $data = json_decode($json);
    $token = $data->Token;
    $id = $data->Id;
    $data = (array) $data->Data;
    $update = array();

    $ruser = $this->muser->getdetails(GetDecryption($token));
    if(empty($ruser)) {
      ShowJsonError('Token tidak valid.');
      return;
    }

    if(!empty($data)) {
      foreach ($data as $key => $value) {
        if(!empty($data[$key]) && !array_key_exists($key, $update)) $update[$key] = $value;
      }
    }

    $res = false;
    if(!empty($update)) {
      $update[COL_TGL_UPDATE] = date('Y-m-d H:i:s');
      $res = $this->db->where(COL_ID, $id)->update(TBL_TBL_KELUAR_MASUK, $update);
    }

    if($res) {
      ShowJsonSuccess('SUCCESS');
      return;
    } else {
      ShowJsonError('GAGAL');
      return;
    }
  }
}
