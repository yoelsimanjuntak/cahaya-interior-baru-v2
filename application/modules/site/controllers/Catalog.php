<?php
class Catalog extends MY_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('mpost');
    ini_set('upload_max_filesize', '50M');
    ini_set('post_max_size', '100M');
  }

  function index() {
    if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        redirect('site/home/index');
    }
    $data['title'] = "Katalog";
    $res = $this->db
    ->select('*, (select count(*) from catalog_item i where i.CatalogID = catalog.Uniq) as JlhItem')
    ->order_by(COL_CATALOGSEQ)
    ->get(TBL_CATALOG)
    ->result_array();

    $data['res'] = $res;
    $this->template->load('backend' , 'catalog/index', $data);
  }

  function add() {
    if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        redirect('site/home/index');
    }
    $user = GetLoggedUser();
    $data['title'] = "Tambah Katalog";

    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "gif|jpg|jpeg|png|pdf|mp4|avi|mkv|wav|webm|flv|wmv|mov";
      $config['max_size']	= 10000;
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);

      $arrMedia = array();
      $arrPostImages = $this->input->post('ArrItem');

      $arrAttachment = array();
      $datAttachment = array();
      $files = $_FILES;
      $cpt = !empty($_FILES)&&isset($_FILES['file'])?count($_FILES['file']['name']):0;
      $arrPostImages = json_decode($arrPostImages);

      for($i=0; $i<$cpt; $i++)
      {
        if(!empty($files['file']['name'][$i]) && isset($arrPostImages[$i])) {
          $_FILES['file']['name']= $files['file']['name'][$i];
          $_FILES['file']['type']= $files['file']['type'][$i];
          $_FILES['file']['tmp_name']= $files['file']['tmp_name'][$i];
          $_FILES['file']['error']= $files['file']['error'][$i];
          $_FILES['file']['size']= $files['file']['size'][$i];

          if($this->upload->do_upload('file')) {
            $upl = $this->upload->data();

            if(!empty($arrPostImages[$i])) {
              $img = $arrPostImages[$i];
              $arrMedia[] = array(
                COL_ITEMFILE=>$upl['file_name'],
                COL_ITEMNAME=>$img->ItemName,
                COL_ITEMDESC=>$img->ItemDesc,
                COL_ITEMGRID=>$img->ItemGrid
              );
            }
          } else {
            ShowJsonError('GAGAL MENGUNGGAH FILE #'.($i+1).': '.strip_tags($this->upload->display_errors()));
            exit();
          }

        }
      }

      $data = array(
        COL_CATALOGNAME => $this->input->post(COL_CATALOGNAME),
        COL_CATALOGSEQ => $this->input->post(COL_CATALOGSEQ),
        COL_CATALOGDESC => $this->input->post(COL_CATALOGDESC),
        COL_CATALOGGRID => $this->input->post(COL_CATALOGGRID)
      );

      if(isset($_FILES['thumbnail'])) {
        if($this->upload->do_upload('thumbnail')) {
          $upl = $this->upload->data();

          $data[COL_CATALOGTHUMB] = $upl['file_name'];
        } else {
          ShowJsonError('GAGAL MENGUNGGAH THUMBNAIL: '.strip_tags($this->upload->display_errors()));
          exit();
        }
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_CATALOG, $data);
        if(!$res) {
          throw new Exception('GAGAL MENGINPUT KATALOG.');
        }

        $id = $this->db->insert_id();
        if(!empty($arrMedia)){
          $id = $this->db->insert_id();
          for($i=0; $i<count($arrMedia); $i++) {
            $arrMedia[$i][COL_CATALOGID]=$id;
          }

          $res = $this->db->insert_batch(TBL_CATALOG_ITEM, $arrMedia);
          if(!$res) {
            throw new Exception('GAGAL MENGINPUT ITEM.');
          }
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

      $this->db->trans_commit();
      ShowJsonSuccess('KATALOG BERHASIL DIINPUT.', array('redirect'=>site_url('site/catalog/index')));
      exit();
    } else {
      $this->template->load('backend' , 'catalog/form', $data);
    }
  }

  function edit($id) {
    if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        redirect('site/home/index');
    }
    $user = GetLoggedUser();
    $data['title'] = "Ubah Katalog";

    $data['data'] = $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_CATALOG)->row_array();
    if(empty($rdata)){
      show_404();
      return;
    }

    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "gif|jpg|jpeg|png|pdf|mp4|avi|mkv|wav|webm|flv|wmv|mov";
      $config['max_size']	= 10000;
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);

      $data['data'] = $_POST;

      $arrMedia = array();
      $arrPostImages = $this->input->post('ArrItem');
      $arrPostImagesExist = $this->input->post('fileExist');

      $arrAttachment = array();
      $datAttachment = array();
      $files = $_FILES;
      $cpt = !empty($_FILES)&&isset($_FILES['file'])?count($_FILES['file']['name']):0;
      $arrPostImages = json_decode($arrPostImages);

      for($i=0; $i<$cpt; $i++)
      {
        if(!empty($files['file']['name'][$i]) && isset($arrPostImages[($i+count($arrPostImagesExist))])) {
          $_FILES['file']['name']= $files['file']['name'][$i];
          $_FILES['file']['type']= $files['file']['type'][$i];
          $_FILES['file']['tmp_name']= $files['file']['tmp_name'][$i];
          $_FILES['file']['error']= $files['file']['error'][$i];
          $_FILES['file']['size']= $files['file']['size'][$i];

          if($this->upload->do_upload('file')) {
            $upl = $this->upload->data();

            if(!empty($arrPostImages[($i+count($arrPostImagesExist))])) {
              $img = $arrPostImages[($i+count($arrPostImagesExist))];
              $arrMedia[] = array(
                COL_CATALOGID=>$id,
                COL_ITEMFILE=>$upl['file_name'],
                COL_ITEMNAME=>$img->ItemName,
                COL_ITEMDESC=>$img->ItemDesc,
                COL_ITEMGRID=>$img->ItemGrid
              );
            }
          } else {
            ShowJsonError('GAGAL MENGUNGGAH FILE #'.($i+1).': '.strip_tags($this->upload->display_errors()));
            exit();
          }
        }
      }

      $data = array(
        COL_CATALOGNAME => $this->input->post(COL_CATALOGNAME),
        COL_CATALOGSEQ => $this->input->post(COL_CATALOGSEQ),
        COL_CATALOGDESC => $this->input->post(COL_CATALOGDESC),
        COL_CATALOGGRID => $this->input->post(COL_CATALOGGRID)
      );

      if(isset($_FILES['thumbnail'])) {
        if($this->upload->do_upload('thumbnail')) {
          $upl = $this->upload->data();

          $data[COL_CATALOGTHUMB] = $upl['file_name'];
        } else {
          ShowJsonError('GAGAL MENGUNGGAH THUMBNAIL: '.strip_tags($this->upload->display_errors()));
          exit();
        }
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_CATALOG, $data);
        if(!$res) {
          throw new Exception('GAGAL MEMPERBAHARUI KATALOG.');
        }

        $res = $this->db->where(COL_CATALOGID, $id)->where_not_in(COL_UNIQ, $arrPostImagesExist)->delete(TBL_CATALOG_ITEM);
        if(!$res) {
          throw new Exception('GAGAL MEMPERBAHARUI KATALOG.');
        }

        if(!empty($arrMedia)){
          $res = $this->db->insert_batch(TBL_CATALOG_ITEM, $arrMedia);
          if(!$res) {
            throw new Exception('GAGAL MEMPERBAHARUI KATALOG.');
          }
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }

      $this->db->trans_commit();
      ShowJsonSuccess('KATALOG BERHASIL DIPERBAHARUI.', array('redirect'=>site_url('site/catalog/index/')));
      exit();
    } else {
      $this->template->load('backend' , 'catalog/form', $data);
    }
  }

  function delete(){
    if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
      redirect('site/home/index');
    }
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      $files = $this->db->where(COL_CATALOGID, $datum)->get(TBL_CATALOG_ITEM)->result_array();
      if(!empty($files)) {
        foreach($files as $f) {
          if(file_exists(MY_UPLOADPATH.$f[COL_ITEMFILE])) {
            unlink(MY_UPLOADPATH.$f[COL_ITEMFILE]);
          }
        }
      }

      $this->db->delete(TBL_CATALOG, array(COL_UNIQ => $datum));
      $deleted++;
    }
    if($deleted){
      ShowJsonSuccess($deleted." DATA BERHASIL DIHAPUS");
    }else{
      ShowJsonError("TIDAK ADA DATA YANG DIHAPUS!");
    }
  }
}
