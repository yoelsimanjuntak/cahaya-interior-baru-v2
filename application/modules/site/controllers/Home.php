<?php
class Home extends MY_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('mpost');
  }

  public function index() {
    //redirect('site/user/login');
    $data['title'] = 'Beranda';
		$this->template->load('moso' , 'home/index', $data);
  }

  public function page($slug) {
    $this->load->model('mpost');
    $data['title'] = 'Page';

    $this->db->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner");
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner");
    $this->db->where("(".TBL__POSTS.".".COL_POSTSLUG." = '".$slug."' OR ".TBL__POSTS.".".COL_POSTID." = '".$slug."')");
    $rpost = $this->db->get(TBL__POSTS)->row_array();
    if(!$rpost) {
      show_404();
      return false;
    }
    $data['data'] = $rpost;
    $data['title'] = $rpost[COL_POSTTITLE];

    $rheader = $this->db
    ->where(COL_ISTHUMBNAIL, 1)
    ->where(COL_POSTID, $rpost[COL_POSTID])
    ->get(TBL__POSTIMAGES)
    ->row_array();
    if(!empty($rheader)) {
      $data['ogimg'] = MY_UPLOADURL.$rheader[COL_IMGPATH];
    }

    $strippedcontent = strip_tags($rpost[COL_POSTCONTENT]);
    $data['ogdesc'] = strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent;
    $data['ogtitle'] = ucwords($rpost[COL_POSTTITLE]).' | '.$this->setting_web_name;

    $this->db->insert(TBL_LOGS, array(COL_POSTID=> $rpost[COL_POSTID], COL_LOGURL=>current_url(), COL_LOGTIMESTAMP=>date('Y-m-d H:i:s')));
    $this->template->load('main', 'home/page', $data);
  }

  public function search() {
    $data['title'] = 'Pencarian';
    $qcond = '1=1';
    $qkeyword = isset($_GET['q'])?$_GET['q']:'';
    $qtag = isset($_GET['tag'])?$_GET['tag']:'';
    $qkategori = isset($_GET['cat'])?$_GET['cat']:'';
    $qsort = isset($_GET['sort'])?$_GET['sort']:'';
    $qpage = isset($_GET['page'])?$_GET['page']:1;

    if(!empty($qkategori)) {
      if(is_array($qkategori)) $qcond .= ' and _posts.PostCategoryID in ('.implode(",", $qkategori).')';
      else $qcond .= " and _posts.PostCategoryID = $qkategori";
    }
    if(!empty($qkeyword)) {
      $qcond .= " and (_posts.PostTitle like '%$qkeyword%' or _posts.PostContent like '%$qkeyword%')";
    }
    if(!empty($qkeyword)) {
      $qcond .= " and _posts.PostMetaTags like '%$qtag%'";
    }

    if(!empty($qpage)&&$qpage>1) {
      $this->db->limit(10, (($qpage-1)*10));
    } else {
      $this->db->limit(10);
    }

    $rterkini = $this->db
    ->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner")
    ->where($qcond)
    ->order_by(COL_POSTDATE, 'desc')
    ->get(TBL__POSTS)
    ->result_array();

    $data['data'] = $rterkini;
    $this->template->load('frontend-magz', 'home/search', $data);
  }

  public function _404() {
    $data['title'] = 'Error';
    if(IsLogin()) {
      $this->template->load('backend' , 'home/_error', $data);
    } else {
      $this->template->load('frontend' , 'home/_error', $data);
    }
  }

  public function post($cat=null) {
    $data['title'] = 'Tautan';
    $start = !empty($_GET['start'])&&$_GET['start']>0?$_GET['start']:0;
    $rcat = $this->db
    ->where(COL_POSTCATEGORYID, $cat)
    ->get(TBL__POSTCATEGORIES)
    ->row_array();
    if(empty($rcat)) {
      show_404();
      return false;
    }

    if(!empty($_GET['dateFrom'])) $this->db->where(TBL__POSTS.".".COL_CREATEDON." >= ", $_GET['dateFrom']);
    if(!empty($_GET['dateTo'])) $this->db->where(TBL__POSTS.".".COL_CREATEDON." <= ", $_GET['dateTo']);
    if(!empty($_GET['keyword'])) {
      $this->db->group_start();
      $this->db->like(TBL__POSTS.".".COL_POSTCONTENT, $_GET['keyword']);
      $this->db->or_like(TBL__POSTS.".".COL_POSTTITLE, $_GET['keyword']);
      $this->db->group_end();
    }

    $q = $this->db
    ->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner")
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner")
    ->where(TBL__POSTS.".".COL_POSTCATEGORYID, $cat)
    ->order_by(TBL__POSTS.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL__POSTS, TRUE);

    $data['res'] = $rpost = $this->db->query($q)->result_array();
    $data['count'] = $this->db->query($q)->num_rows();
    $data['start'] = $start;

    if(!empty($rpost)) {
      $data['title'] = $rpost[0][COL_POSTCATEGORYNAME];
    }

    $data['data'] = $rpost;
    $data['rcat'] = $rcat;
    $this->template->load('main', 'home/post', $data);
  }

  public function gallery($id=null) {
    $data['title'] = 'Katalog';
    $data['id'] = $id;
    $data['res'] = $rkatalog = $this->db
    ->select('*, (select count(*) from catalog_item i where i.CatalogID = catalog.Uniq) as JlhItem')
    ->order_by(COL_CATALOGSEQ)
    ->get(TBL_CATALOG)
    ->result_array();

    if(!empty($id)) {
      $rcatalog = $this->db->where(COL_UNIQ, $id)->get(TBL_CATALOG)->row_array();
      if(!empty($rcatalog)) {
        $data['title'] = $rcatalog[COL_CATALOGNAME];
      }

      $data['res'] = $this->db->where(COL_CATALOGID, $id)->get(TBL_CATALOG_ITEM)->result_array();
    }

    $this->template->load('main' , 'home/catalog', $data);
  }
}
 ?>
