<?php
$arrItem = array();
$item_ = array();
if(!empty($data[COL_UNIQ])) {
  $item_ = $this->db->where(COL_CATALOGID, $data[COL_UNIQ])->get(TBL_CATALOG_ITEM)->result_array();
  foreach($item_ as $f) {
    $fPath = MY_UPLOADPATH.$f[COL_ITEMFILE];
    if(file_exists($fPath)) {
      $fType = pathinfo($fPath, PATHINFO_EXTENSION);
      $fData = file_get_contents($fPath);
      $fBase64 = 'data:image/'. $fType.';base64,'. base64_encode($fData);
      $arrItem[] = array(
        'Uniq'=>$f[COL_UNIQ],
        'ItemURL'=>MY_UPLOADURL.$f[COL_ITEMFILE],
        'ItemName'=>$f[COL_ITEMNAME],
        'ItemDesc'=>$f[COL_ITEMDESC],
        'ItemGrid'=>$f[COL_ITEMGRID]
      );
    }

  }
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark font-weight-light"><?= $title ?></h1>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-default">
          <div class="card-header">
            <a href="<?=site_url('site/catalog/index')?>" class="btn btn-secondary btn-sm"><i class="far fa-arrow-circle-left"></i>&nbsp;KEMBALI</a>&nbsp;
            <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-8">
                <div class="form-group row">
                  <label class="control-label col-sm-4">NAMA</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="<?=COL_CATALOGNAME?>" value="<?=!empty($data) ? $data[COL_CATALOGNAME] : ''?>" placeholder="NAMA KATALOG" required />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-4">NO. URUT / GRID CLASS</label>
                  <div class="col-sm-3">
                    <input type="number" class="form-control" name="<?=COL_CATALOGSEQ?>" value="<?=!empty($data) ? $data[COL_CATALOGSEQ] : ''?>" placeholder="1" required />
                  </div>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" name="<?=COL_CATALOGGRID?>" value="<?=!empty($data) ? $data[COL_CATALOGGRID] : ''?>" placeholder="col-md-6" />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-sm-4">KETERANGAN</label>
                  <div class="col-sm-6">
                    <textarea name="<?=COL_CATALOGDESC?>" class="form-control"><?=!empty($data) ? $data[COL_CATALOGDESC] : ''?></textarea>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <label>THUMBNAIL</label>
                  <input type="hidden" name="<?=COL_CATALOGTHUMB?>" value="<?=!empty($data)&&!empty($data[COL_CATALOGTHUMB])?MY_UPLOADURL.$data[COL_CATALOGTHUMB]:''?>" />
                  <div class="row">
                    <div class="col-sm-10 p-2" style="border: 1px solid #dedede; border-radius: .25rem">
                      <div id="div-thumb-preview" class="row p-2" style="justify-content: center">
                        <div class="d-none preview-blueprint ml-2" style="width: 150px; height: 100px; border: 1px solid #dedede; flex-direction: column">
                          <div class="row m-1">
                            <div class="col-sm-12 p-0 div-info">
                            </div>
                          </div>
                          <div class="row m-1 mt-auto">
                            <div class="col-sm-12 p-0">
                              <button type="button" class="btn btn-xs btn-danger btn-block btn-del-thumbnail"><i class="far fa-times-circle"></i></button>
                            </div>
                          </div>
                        </div>
                        <button type="button" id="btn-add-thumbnail" class="btn btn-app btn-lg m-0 ml-2" style="height: 100px; font-size: 14px">
                          <i class="far fa-image"></i>PILIH
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card card-outline card-default">
          <div class="card-header">
            <h5 class="card-title font-weight-bold">ITEM</h5>
          </div>
          <div class="card-body">
            <input type="hidden" name="ArrItem" value='<?=json_encode($arrItem)?>' />
            <div class="row">
              <div class="col-sm-12">
                <div class="col-sm-12 p-2" style="border: 1px solid #dedede; border-radius: .25rem">
                  <div id="div-item-preview" class="row p-2">
                    <div class="d-none preview-blueprint ml-2" style="width: 150px;height: 100px; border: 1px solid #dedede; flex-direction: column">
                      <div class="row m-1">
                        <div class="col-sm-12 p-0 div-info">
                        </div>
                      </div>
                      <div class="row m-1 mt-auto">
                        <div class="col-sm-12 p-0">
                          <button type="button" class="btn btn-xs btn-danger btn-block btn-del-item"><i class="far fa-times-circle"></i></button>
                        </div>
                      </div>
                    </div>
                    <button type="button" id="btn-add-item" class="btn btn-app btn-lg m-0 ml-2" style="height: 100px; font-size: 14px">
                      <i class="fas fa-plus-circle"></i>TAMBAH
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
    foreach($item_ as $f) {
      ?>
      <input type="hidden" name="fileExist[]" value="<?=$f[COL_UNIQ]?>" />
      <?php
    }
    ?>
    <?=form_close()?>
  </div>
</section>
<div class="modal fade" id="modal-thumbnail" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">UNGGAH THUMBNAIL</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>FILE</label>
          <div class="custom-file">
            <input name="ThumbnailFile" class="custom-file-input" type="file" onchange="thumbnailPreview()" accept="image/*">
            <label class="custom-file-label" for="file">PILIH FILE</label>
          </div>
        </div>
        <p class="text-center d-none">
          <img class="preview text-center" src="" style="max-width: 100%; height: 300px !important" alt="Preview">
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;TUTUP</button>
        <button type="button" class="btn btn-sm btn-primary" id="btn-insert-thumbnail"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-item" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">UNGGAH MEDIA</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>FILE</label>
          <div class="custom-file">
            <input name="ItemFile" class="custom-file-input" type="file" onchange="itemPreview()" accept="image/*">
            <label class="custom-file-label" for="file">PILIH FILE</label>
          </div>
        </div>
        <div class="form-group">
          <label>NAMA / GRID</label>
          <div class="row">
            <div class="col-sm-8">
              <input type="text" name="ItemName" class="form-control" placeholder="NAMA ITEM" />
            </div>
            <div class="col-sm-4">
              <input type="text" name="ItemGrid" class="form-control" placeholder="GRID" />
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>KETERANGAN</label>
          <textarea name="ItemDesc" class="form-control"></textarea>
        </div>
        <p class="d-none">
          <img class="preview text-center" src="" style="max-width: 100%; display: none" alt="Preview">
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;TUTUP</button>
        <button type="button" class="btn btn-sm btn-primary" id="btn-insert-item"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalThumbnail = $('#modal-thumbnail');
var modalItem = $('#modal-item');

function thumbnailPreview() {
  const preview = $('img.preview', modalThumbnail);
  const file = $("[name=ThumbnailFile]").prop('files')[0];
  const reader = new FileReader();

  reader.addEventListener("load", function () {
    preview.attr('src', reader.result);
    preview.closest('p').removeClass('d-none');
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
}

function itemPreview() {
  const preview = $('img.preview', modalItem);
  const file = $("[name=ItemFile]").prop('files')[0];
  const reader = new FileReader();

  reader.addEventListener("load", function () {
    preview.attr('src', reader.result);
    preview.closest('p').removeClass('d-none');
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
}

function addItem(obj) {
  var arr = $('[name=ArrItem]').val();
  if(arr) {
    arr = JSON.parse(arr);
  } else {
    arr = [];
  }
  arr.push(obj);
  $('[name=ArrItem]').val(JSON.stringify(arr)).trigger('change');
}

$(document).ready(function() {
  bsCustomFileInput.init();

  modalThumbnail.on('hidden.bs.modal', function (e) {
    $('img.preview', modalThumbnail).hide();
    $('img.preview', modalThumbnail).attr('src', '');
    $("[name=ThumbnailFile]", modalThumbnail).val('').trigger('change');
    $("[name=ThumbnailFile]", modalThumbnail).next('label').html('PILIH FILE');
  });
  modalItem.on('hidden.bs.modal', function (e) {
    $('img.preview', modalItem).hide();
    $('img.preview', modalItem).attr('src', '');
    $("[name=ItemFile]", modalItem).val('').trigger('change');
    $("[name=ItemFile]", modalItem).next('label').html('PILIH FILE');
    $('[name=ItemName]', modalItem).val('');
    $('[name=ItemDesc]', modalItem).val('');
    $('[name=ItemGrid]', modalItem).val('');
  });

  $("#btn-add-thumbnail").click(function(){
    modalThumbnail.modal('show');
  });
  $("#btn-add-item").click(function(){
    modalItem.modal('show');
  });

  $('#btn-insert-thumbnail', modalThumbnail).click(function() {
    var file = $('img.preview', modalThumbnail).attr('src');

    $('[name=CatalogThumb]').val(file).trigger('change');
    $('#form-main').append($("input[name=ThumbnailFile][type=file]", modalThumbnail).clone().removeClass('custom-file-input').attr('name','thumbnail').removeAttr('onchange'));
    modalThumbnail.modal('hide');
  });

  $('#btn-insert-item', modalItem).click(function() {
    var objMedia = {};
    var mediaFile = $('img.preview', modalItem).attr('src');
    var mediaName = $('[name=ItemName]', modalItem).val();
    var mediaDesc = $('[name=ItemDesc]', modalItem).val();
    var mediaGrid = $('[name=ItemGrid]', modalItem).val();
    var uniq_ = moment().format('YYYYMMDDHHmmss');

    objMedia = {
      Uniq: uniq_,
      ItemPath: mediaFile,
      ItemName: mediaName,
      ItemDesc: mediaDesc,
      ItemGrid: mediaGrid,
    };

    addItem(objMedia);
    $('#form-main').append($("input[name=ItemFile][type=file]", modalItem).clone().removeClass('custom-file-input').attr('name','file[]').removeAttr('onchange'));
    modalItem.modal('hide');
  });

  $('[name=CatalogThumb]').change(function() {
    var src = $('[name=CatalogThumb]').val();
    $('.preview', $('#div-thumb-preview')).remove();
    if(src) {
      var imgEl = $('.preview-blueprint', $('#div-thumb-preview')).clone();

      imgEl.removeClass('preview-blueprint').removeClass('d-none').addClass('d-flex').addClass('preview');
      imgEl.css('background-image', "url('"+src+"')");
      imgEl.css('background-size', 'contain');
      imgEl.css('background-repeat', 'no-repeat');
      imgEl.css('background-position', 'center');

      $('button.btn-del-thumbnail', imgEl).click(function(){
        $('[name=CatalogThumb]').val('').trigger('change');
        $("#btn-add-thumbnail").toggle();
      });

      $('#div-thumb-preview').prepend(imgEl);
      $("#btn-add-thumbnail").toggle();
    }
  }).trigger('change');

  $('[name=ArrItem]').change(function() {
    var arr = $('[name=ArrItem]').val();
    if(arr) {
      arr = JSON.parse(arr);
    } else {
      arr = [];
    }

    if(arr) {
      $('.preview', $('#div-item-preview')).remove();
      for(var i=0; i<arr.length; i++) {
        var imgEl = $('.preview-blueprint', $('#div-item-preview')).clone();
        imgEl.removeClass('preview-blueprint').removeClass('d-none').addClass('d-flex').addClass('preview');
        if(arr[i].ItemURL) {
          imgEl.css('background-image', "url('"+arr[i].ItemURL+"')");
        } else {
          imgEl.css('background-image', "url('"+arr[i].ItemPath+"')");
        }
        imgEl.css('background-size', 'contain');
        imgEl.css('background-repeat', 'no-repeat');
        imgEl.css('background-position', 'center');

        $('button.btn-del-item', imgEl).data('uniq', arr[i].Uniq);
        $('button.btn-del-item', imgEl).click(function(){
          var uniq = $(this).data('uniq');
          var arrImg_ = $('[name=ArrItem]').val();
          if(arrImg_) {
            arrImg_ = JSON.parse(arrImg_);
          }
          else {
            arrImg_ = [];
          }

          arrImg_ = arrImg_.filter(function(obj) {
            return obj.Uniq != uniq;
          });

          $('[name=ArrItem]').val(JSON.stringify(arrImg_)).trigger('change');
          $('[name^=fileExist][value='+uniq+']', $('#form-main')).remove();
        });

        if(i==0) {
          $('#div-item-preview').prepend(imgEl);
        } else {
          $("#div-item-preview > div.preview:nth-child("+i+")").after(imgEl);
        }
      }
    }
  }).trigger('change');

  $('button.btn-del-media').click(function(){
    var uniq = $(this).data('uniq');
    var arrImg_ = $('[name=PostImages]').val();
    var arrImg2_ = $('[name=PostImages2]').val();
    if(arrImg_) {
      arrImg_ = JSON.parse(arrImg_);
      arrImg2_ = JSON.parse(arrImg2_);
    }
    else {
      arrImg_ = [];
      arrImg2_ = [];
    }

    arrImg_ = arrImg_.filter(function(obj) {
      return obj.Uniq != uniq;
    });
    arrImg2_ = arrImg2_.filter(function(obj) {
      return obj.Uniq != uniq;
    });

    $('[name=PostImages]').val(JSON.stringify(arrImg_)).trigger('change');
    $('[name=PostImages2]').val(JSON.stringify(arrImg2_)).trigger('change');
    $('[name^=fileExist][value='+uniq+']', $('#form-main')).remove();
  });

  $('#form-main').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              location.href = res.redirect;
            } else {
              setTimeout(function(){
                location.reload();
              }, 1000);
            }
          }
        },
        error: function(data) {
          console.log(data)
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

});
</script>
