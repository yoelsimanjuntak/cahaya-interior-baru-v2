<?php
$qkeyword = isset($_GET['q'])?$_GET['q']:'';
$qtag = isset($_GET['tag'])?$_GET['tag']:'';
$qkategori = isset($_GET['cat'])?$_GET['cat']:'';
$qsort = isset($_GET['sort'])?$_GET['sort']:'';
$qpage = isset($_GET['page'])?$_GET['page']:1;

$rkategori = $this->db
->order_by(COL_SEQ)
->get(TBL__POSTCATEGORIES)
->result_array();
?>
<section class="search">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <form id="form-search" action="<?=current_url()?>" method="get">
          <aside>
            <h2 class="aside-title">Kata Kunci</h2>
            <div class="aside-body">
              <p>Ketik kata kunci yang ingin anda cari dibawah ini.</p>
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="q" class="form-control" placeholder="Kata Kunci" value="<?=!empty($qkeyword)?$qkeyword:''?>">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary">
                      <i class="ion-search"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </aside>
          <!--<aside>
            <h2 class="aside-title">Tag</h2>
            <div class="aside-body">
              <p>Pilih tag untuk memudahkan pencarian anda.</p>
              <div class="form-group">
                <select class="form-control" size="1">
                  <option>Waktu</option>
                  <option>Popularitas</option>
                </select>
              </div>
            </div>
          </aside>-->
          <aside>
            <h2 class="aside-title">Kategori</h2>
            <div class="aside-body">
              <div class="checkbox-group">
                <div class="form-group">
                  <label><input type="checkbox" name="cat[]" value="-1" <?=empty($qkategori)?'checked':''?>> Semua</label>
                </div>
                <?php
                foreach($rkategori as $kat) {
                  $checked = false;
                  if(!empty($qkategori)) {
                    if(is_array($qkategori) && in_array($kat[COL_POSTCATEGORYID], $qkategori)) $checked=true;
                    else if($kat[COL_POSTCATEGORYID]==$qkategori) $checked=true;
                  }
                  ?>
                  <div class="form-group">
                    <label><input type="checkbox" name="cat[]" value="<?=$kat[COL_POSTCATEGORYID]?>" <?=$checked?'checked':''?>> <?=$kat[COL_POSTCATEGORYNAME]?></label>
                  </div>
                  <?php
                }
                ?>
              </div>
            </div>
          </aside>
          <!--<aside>
            <h2 class="aside-title">Urut Berdasarkan</h2>
            <div class="aside-body">
              <div class="form-group">
                <select name="sort" class="form-control" size="1">
                  <option value="date">Waktu</option>
                  <option value="pop">Popularitas</option>
                </select>
              </div>
            </div>
          </aside>-->
          <input type="hidden" name="page" value="<?=$qpage?>"/>
        </form>

      </div>
      <div class="col-md-9">
        <div class="row">
          <?php
          if(empty($data)) {
            ?>
            <div class="col-md-12 text-center" style="padding-top: 20px; margin-bottom: 50px;">
              Mohon maaf, data yang anda cari tidak tersedia untuk saat ini.
						</div>
            <?php
          }
          foreach($data as $r) {
            $postID = $r[COL_POSTID];
            $rthumbnail = $this->db
            ->where(COL_ISTHUMBNAIL, 1)
            ->where(COL_POSTID, $postID)
            ->get(TBL__POSTIMAGES)
            ->row_array();

            $strippedcontent = htmlspecialchars_decode(strip_tags($r[COL_POSTCONTENT]));

            echo @'
            <article class="col-md-12 article-list">
              <div class="inner">
                <figure>
                  <a href="single.html">
                    <img src="'.(!empty($rthumbnail)?MY_UPLOADURL.$rthumbnail[COL_IMGPATH]:MY_IMAGEURL.'logo-full.jpg').'">
                  </a>
                </figure>
                <div class="details">
                  <div class="detail" style="font-size: 12px !important">
                    <time>'.(date("d-m-Y", strtotime($r[COL_POSTDATE]))).'</time>&nbsp;
                    <div class="category" style="margin: 0 10px">
                      <a href="'.(site_url('site/home/search').'?cat='.$r[COL_POSTCATEGORYID]).'">'.$r[COL_POSTCATEGORYNAME].'</a>
                    </div>

                  </div>
                  <h1><a href="'.(site_url('site/home/page/'.$r[COL_POSTSLUG])).'">'.$r[COL_POSTTITLE].'</a></h1>
                  <p>'.(strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent).'</p>
                  <footer>
                    <a class="btn btn-primary more" href="'.(site_url('site/home/page/'.$r[COL_POSTSLUG])).'">
                      <div><small>SELENGKAPNYA</small></div>
                      <div><i class="ion-ios-arrow-thin-right"></i></div>
                    </a>
                  </footer>
                </div>
              </div>
            </article>';
          }
          ?>
          <div class="col-md-12 text-center">
            <button type="button" id="btn-next" data-value="<?=$qpage>1?$qpage-1:1?>" class="btn btn-primary <?=$qpage>1?'':'disabled'?>"><small><i class="far fa-chevron-left"></i> SEBELUMNYA</small></button>
            <button type="button" id="btn-prev" data-value="<?=$qpage+1?>" class="btn btn-primary <?=count($data)<10?'disabled':''?>"><small>SELANJUTNYA <i class="far fa-chevron-right"></i></small></button>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  $('#btn-next, #btn-prev').not('.disabled').click(function() {
    var pageno = $(this).data('value');
    $('[name=page]', $('#form-search')).val(pageno);
    $('#form-search').submit();
  });

  $('[name^=cat]').change(function(){
    if($(this).val()==-1) {
      $('[name^=cat]:checked').prop('checked', false);
    } else {
      $('[name^=cat][value=-1]').prop('checked', false);
    }
    $('#form-search').submit();
  });

  $('#form-search').on('submit', function(){
    $('[name^=cat][value=-1]').prop('checked', false);
  });
});
</script>
