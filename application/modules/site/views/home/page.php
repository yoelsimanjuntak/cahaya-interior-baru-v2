<?php
$qpopular = @"
select * from (
select *, (select count(*) from logs where logs.PostID = p.PostID) as HitCount
from _posts p
) tbl
order by tbl.HitCount desc, tbl.PostDate desc
limit 10
";
$rpopular = $this->db->query($qpopular)->result_array();

$rheader = $this->db
->where(COL_ISHEADER, 1)
->where(COL_POSTID, $data[COL_POSTID])
->get(TBL__POSTIMAGES)
->row_array();

$postContent = $data[COL_POSTCONTENT];
$rimg = $this->db
->where(COL_ISHEADER.' != ', 1)
->where(COL_POSTID, $data[COL_POSTID])
->get(TBL__POSTIMAGES)
->result_array();
foreach($rimg as $img) {
  if(!empty($img[COL_IMGSHORTCODE])) {
    $postContent = str_replace($img[COL_IMGSHORTCODE], '<figure class="text-center"><img src="'.MY_UPLOADURL.$img[COL_IMGPATH].'" style="max-height: 400px; max-width: 100%" /></figure><small style="font-size: 10px; font-style:italic;">'.$img[COL_IMGDESC].'</small>', $postContent);
  }
}

$arrTags = array();
if(!empty($data[COL_POSTMETATAGS])) {
  $arrTags = explode(",", $data[COL_POSTMETATAGS]);
}
?>
<div class="colorlib-blog">
  <div class="colorlib-narrow-content">
    <div class="row">
      <div class="col-md-12 animate-box fadeInLeft animated" data-animate-effect="fadeInLeft">
        <span class="heading-meta"><?=$data[COL_POSTCATEGORYNAME]?></span>
        <h2 class="colorlib-heading"><?=$data[COL_POSTTITLE]?></h2>
      </div>
    </div>
    <div class="row">
      <figure class="text-center">
        <img src="<?=MY_UPLOADURL.$rheader[COL_IMGPATH]?>" style="max-height: 400px; max-width: 100%" />
      </figure>
      <?=$postContent?>
    </div>
    <div class="row">
      <div id="disqus_thread"></div>
    </div>
  </div>
</div>
<script>

    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
    /*
     */
    var disqus_config = function () {
        this.page.url = '<?=site_url('site/home/page/'.$data[COL_POSTSLUG])?>';  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = '<?=$data[COL_POSTSLUG]?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    (function() { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        s.src = '<?=$this->setting_web_disqus_url?>';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
