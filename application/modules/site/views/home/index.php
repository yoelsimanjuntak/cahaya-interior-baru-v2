<?php
$rkatalog = $this->db
->select('*, (select count(*) from catalog_item i where i.CatalogID = catalog.Uniq) as JlhItem')
->order_by(COL_CATALOGSEQ)
->get(TBL_CATALOG)
->result_array();

$rkonten = $this->mpost->getall(null);
?>
<aside id="colorlib-hero" class="js-fullheight">
  <div class="flexslider js-fullheight">
    <ul class="slides">
      <li style="background-image: url('<?=MY_IMAGEURL.'slide/carousel-1.jpg'?>');">
        <div class="overlay"></div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 col-md-push-3 col-sm-12 col-xs-12 js-fullheight slider-text">
              <div class="slider-text-inner">
                <div class="desc">
                  <h1>Kualitas Terjamin</h1>
                  <h2>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </li>
      <li style="background-image: url('<?=MY_IMAGEURL.'slide/carousel-2.jpg'?>');">
        <div class="overlay"></div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 col-md-push-3 col-sm-12 col-xs-12 js-fullheight slider-text">
              <div class="slider-text-inner">
                <div class="desc">
                  <h1>Layanan Memuaskan</h1>
                  <h2>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  </h2>
              </div>
            </div>
          </div>
        </div>
      </li>
      <li style="background-image: url('<?=MY_IMAGEURL.'slide/carousel-3.jpg'?>');">
        <div class="overlay"></div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 col-md-push-3 col-sm-12 col-xs-12 js-fullheight slider-text">
              <div class="slider-text-inner">
                <div class="desc">
                  <h1>Pilihan Lengkap</h1>
                  <h2>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  </h2>
              </div>
            </div>
          </div>
        </div>
      </li>
      </ul>
    </div>
</aside>

<div class="colorlib-about">
  <div class="colorlib-narrow-content">
    <div class="row">
      <div class="col-md-6">
        <div class="about-img animate-box" data-animate-effect="fadeInLeft" style="background-image: url('<?=MY_IMAGEURL.'home-1.jpg'?>');">
        </div>
      </div>
      <div class="col-md-6 animate-box" data-animate-effect="fadeInLeft">
        <div class="about-desc">
          <span class="heading-meta">SELAMAT DATANG DI</span>
          <h2 class="colorlib-heading">CAHAYA INTERIOR BARU</h2>
          <p class="text-justify">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </div>
        <div class="row padding">
          <div class="col-md-4 no-gutters animate-box" data-animate-effect="fadeInLeft">
            <div class="steps active">
              <p class="icon"><span><i class="icon-check" style="font-weight: bold"></i></span></p>
              <h3>KUALITAS<br>TERJAMIN</h3>
            </div>
          </div>
          <div class="col-md-4 no-gutters animate-box" data-animate-effect="fadeInLeft">
            <div class="steps active">
              <p class="icon"><span><i class="icon-check" style="font-weight: bold"></i></span></p>
              <h3>LAYANAN<br>MEMUASKAN</h3>
            </div>
          </div>
          <div class="col-md-4 no-gutters animate-box" data-animate-effect="fadeInLeft">
            <div class="steps active">
              <p class="icon"><span><i class="icon-check" style="font-weight: bold"></i></span></p>
              <h3>PILIHAN<br>LENGKAP</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="colorlib-services" style="padding-bottom: 0 !important">
  <div class="colorlib-narrow-content">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
        <span class="heading-meta">KATEGORI</span>
        <h2 class="colorlib-heading">JASA YANG DITAWARKAN</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-12">
            <div class="colorlib-feature animate-box" data-animate-effect="fadeInLeft">
              <div class="colorlib-icon">
                <i class="fad fa-brush"></i>
              </div>
              <div class="colorlib-text">
                <h3 style="font-weight: bold">INSTALASI INTERIOR</h3>
                <ul>
                  <li>Bedroom</li>
                  <li>Floor</li>
                  <li>Gordyn</li>
                  <li>Kitchen Set</li>
                  <li>Wallpaper</li>
                  <li>Wall Moulding</li>
                </ul>
              </div>
            </div>

            <div class="colorlib-feature animate-box" data-animate-effect="fadeInLeft">
              <div class="colorlib-icon">
                <i class="fad fa-user-headset"></i>
              </div>
              <div class="colorlib-text">
                <h3 style="font-weight: bold">KONSULTASI</h3>
                <p>
                  Konsultasikan kebutuhan interior properti anda dengan konsultan kami yang datang ke lokasi secara langsung. <strong>GRATIS!</strong>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-6">
            <a href="#" class="services-wrap animate-box" data-animate-effect="fadeInRight">
              <div class="services-img" style="background-image: url('<?=MY_IMAGEURL.'home-2.jpg'?>')"></div>
              <div class="desc">
                <h3>BEDROOM</h3>
              </div>
            </a>
            <a href="#" class="services-wrap animate-box" data-animate-effect="fadeInRight">
              <div class="services-img" style="background-image: url('<?=MY_IMAGEURL.'home-3.jpg'?>')"></div>
              <div class="desc">
                <h3>FLOOR</h3>
              </div>
            </a>
            <a href="#" class="services-wrap animate-box" data-animate-effect="fadeInRight">
              <div class="services-img" style="background-image: url('<?=MY_IMAGEURL.'home-4.jpg'?>')"></div>
              <div class="desc">
                <h3>GORDYN</h3>
              </div>
            </a>
          </div>
          <div class="col-md-6 move-bottom">
            <a href="#" class="services-wrap animate-box" data-animate-effect="fadeInRight">
              <div class="services-img" style="background-image: url('<?=MY_IMAGEURL.'home-5.jpg'?>')"></div>
              <div class="desc">
                <h3>KITCHEN SET</h3>
              </div>
            </a>
            <a href="#" class="services-wrap animate-box" data-animate-effect="fadeInRight">
              <div class="services-img" style="background-image: url('<?=MY_IMAGEURL.'home-6.jpg'?>')"></div>
              <div class="desc">
                <h3>WALL MOULDING</h3>
              </div>
            </a>
            <a href="#" class="services-wrap animate-box" data-animate-effect="fadeInRight">
              <div class="services-img" style="background-image: url('<?=MY_IMAGEURL.'home-7.jpg'?>')"></div>
              <div class="desc">
                <h3>WALLPAPER</h3>
              </div>
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<?php
if(!empty($rkatalog)) {
  ?>
  <div class="colorlib-work">
    <div class="colorlib-narrow-content">
      <div class="row">
        <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
          <h2 class="colorlib-heading animate-box">KATALOG</h2>
        </div>
      </div>
      <div class="row">
        <?php
        foreach($rkatalog as $k) {
          ?>
          <div class="animate-box <?=!empty($k[COL_CATALOGGRID])?$k[COL_CATALOGGRID]:'col-sm-3'?>" data-animate-effect="fadeInLeft">
            <div class="project" style="background-image: url('<?=!empty($k[COL_CATALOGTHUMB])?MY_UPLOADURL.$k[COL_CATALOGTHUMB]:MY_IMAGEURL.'home-default.jpg'?>');">
              <div class="desc">
                <div class="con">
                  <h3><a href="<?=site_url('site/home/gallery/'.$k[COL_UNIQ])?>"><?=$k[COL_CATALOGNAME]?></a></h3>
                  <span><?=$k[COL_CATALOGDESC]?></span>
                  <p class="icon">
                    <span><a href="<?=site_url('site/home/gallery/'.$k[COL_UNIQ])?>"><i class="far fa-images"></i> <?=number_format($k['JlhItem'])?></a></span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
  <?php
}
?>

<?php
if(!empty($rkonten)) {
  ?>
  <div class="colorlib-blog">
    <div class="colorlib-narrow-content">
      <div class="row">
        <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
          <h2 class="colorlib-heading">ARTIKEL</h2>
        </div>
      </div>
      <div class="row">
        <?php
        foreach($rkonten as $k) {
          $rthumbnail = $this->db->where(COL_POSTID, $k[COL_POSTID])->where(COL_ISTHUMBNAIL, 1)->get(TBL__POSTIMAGES)->row_array();
          $strippedcontent = htmlspecialchars_decode(strip_tags($k[COL_POSTCONTENT]));
          ?>
          <div class="col-md-4 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
            <div class="blog-entry">
              <a href="<?=site_url('site/home/page/'.$k[COL_POSTID])?>" class="blog-img"><img src="<?=!empty($rthumbnail)?MY_UPLOADURL.$rthumbnail[COL_IMGPATH]:MY_IMAGEURL.'home-default.jpg'?>" class="img-responsive" alt="<?=$k[COL_POSTTITLE]?>"></a>
              <div class="desc">
                <span><small><?=date('d-m-Y', strtotime($k[COL_POSTDATE]))?></small></span>
                <h3><a href="<?=site_url('site/home/page/'.$k[COL_POSTID])?>"><?=$k[COL_POSTTITLE]?></a></h3>
                <p class="text-justify"><?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?></p>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
  <?php
}
?>


<div id="get-in-touch" class="colorlib-bg-color">
  <div class="colorlib-narrow-content">
    <div class="row">
      <div class="col-md-6 animate-box" data-animate-effect="fadeInLeft">
        <h2>KONTAK</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
        <p class="colorlib-lead">Segera hubungi kami, kami siap melayani anda!</p>
        <p>
          <a href="https://wa.me/<?=$this->setting_org_phone?>" target="_blank" class="btn btn-primary btn-learn" style="padding: 8px 15px !important"><i class="fab fa-whatsapp"></i> ADMIN 1</a>&nbsp;
          <a href="https://wa.me/<?=$this->setting_org_fax?>" target="_blank" class="btn btn-primary btn-learn" style="padding: 8px 15px !important"><i class="fab fa-whatsapp"></i> ADMIN 2</a>
        </p>
      </div>
    </div>
  </div>
</div>
