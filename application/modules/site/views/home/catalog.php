<div class="colorlib-blog">
  <div class="colorlib-narrow-content">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
        <?php
        if(!empty($id)) {
          ?>
          <span class="heading-meta">KATALOG</span>
          <?php
        }
        ?>
        <h2 class="colorlib-heading"><?=$title?></h2>
      </div>
    </div>
    <div class="row row-bottom-padded-md">
      <?php
      if(!empty($id)) {
        foreach($res as $k) {
          ?>
          <div class="animate-box <?=!empty($k[COL_ITEMGRID])?$k[COL_ITEMGRID]:'col-sm-3'?>" data-animate-effect="fadeInLeft">
            <div class="project" style="background-image: url('<?=!empty($k[COL_ITEMFILE])?MY_UPLOADURL.$k[COL_ITEMFILE]:MY_IMAGEURL.'home-default.jpg'?>');">
            </div>
          </div>
          <?php
        }
      } else {
        foreach($res as $k) {
          ?>
          <div class="animate-box <?=!empty($k[COL_CATALOGGRID])?$k[COL_CATALOGGRID]:'col-sm-3'?>" data-animate-effect="fadeInLeft">
            <div class="project" style="background-image: url('<?=!empty($k[COL_CATALOGTHUMB])?MY_UPLOADURL.$k[COL_CATALOGTHUMB]:MY_IMAGEURL.'home-default.jpg'?>');">
              <div class="desc">
                <div class="con">
                  <h3><a href="<?=site_url('site/home/gallery/'.$k[COL_UNIQ])?>"><?=$k[COL_CATALOGNAME]?></a></h3>
                  <span><?=$k[COL_CATALOGDESC]?></span>
                  <p class="icon">
                    <span><a href="<?=site_url('site/home/gallery/'.$k[COL_UNIQ])?>"><i class="far fa-images"></i> <?=number_format($k['JlhItem'])?></a></span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
      }
      ?>
    </div>
  </div>
</div>
