<div class="colorlib-blog">
  <div class="colorlib-narrow-content">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
        <h2 class="colorlib-heading"><?=$title?></h2>
      </div>
    </div>
    <div class="row row-bottom-padded-md">
      <?php
      foreach($res as $k) {
        $rthumbnail = $this->db->where(COL_POSTID, $k[COL_POSTID])->where(COL_ISTHUMBNAIL, 1)->get(TBL__POSTIMAGES)->row_array();
        $strippedcontent = htmlspecialchars_decode(strip_tags($k[COL_POSTCONTENT]));
        ?>
        <div class="col-md-4 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
          <div class="blog-entry">
            <a href="<?=site_url('site/home/page/'.$k[COL_POSTID])?>" class="blog-img"><img src="<?=!empty($rthumbnail)?MY_UPLOADURL.$rthumbnail[COL_IMGPATH]:MY_IMAGEURL.'home-default.jpg'?>" class="img-responsive" alt="<?=$k[COL_POSTTITLE]?>"></a>
            <div class="desc">
              <span><small><?=date('d-m-Y', strtotime($k[COL_POSTDATE]))?></small></span>
              <h3><a href="<?=site_url('site/home/page/'.$k[COL_POSTID])?>"><?=$k[COL_POSTTITLE]?></a></h3>
              <p class="text-justify"><?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?></p>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
