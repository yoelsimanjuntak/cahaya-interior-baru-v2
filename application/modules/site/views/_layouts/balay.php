<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?=$this->setting_web_name.(!empty($title) ? ' - '.$title : '')?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

  <!-- Facebook and Twitter integration -->
	<meta property="og:title" content="<?=$this->setting_web_name.(!empty($title) ? ' - '.$title : '')?>"/>
	<meta property="og:image" content="<?=!empty($this->setting_web_logo) ? $this->setting_web_logo : ''?>"/>
	<meta property="og:url" content="<?=current_url()?>"/>
	<meta property="og:site_name" content="<?=!empty($this->setting_web_name) ? $this->setting_web_name : ''?>"/>
	<meta property="og:description" content="<?=!empty($title) ? $title : ''?>"/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="<?=MY_IMAGEURL?>favicon.ico">

	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">

	<!-- Animate.css -->
	<link rel="stylesheet" href="<?=base_url()?>assets/themes/balay/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?=base_url()?>assets/themes/balay/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?=base_url()?>assets/themes/balay/css/bootstrap.css">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?=base_url()?>assets/themes/balay/css/flexslider.css">
	<!-- Flaticons  -->
	<link rel="stylesheet" href="<?=base_url()?>assets/themes/balay/fonts/flaticon/font/flaticon.css">
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?=base_url()?>assets/themes/balay/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/themes/balay/css/owl.theme.default.min.css">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?=base_url()?>assets/themes/balay/css/style.css">

	<!-- Modernizr JS -->
	<script src="<?=base_url()?>assets/themes/balay/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<div id="colorlib-page">
		<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
		<aside id="colorlib-aside" role="complementary" class="border js-fullheight">
			<h1 id="colorlib-logo"><a href="index.html"><?=$this->setting_web_name?></a></h1>
			<nav id="colorlib-main-menu" role="navigation">
				<ul>
					<li <?=current_url()==site_url()?'class="colorlib-active"':''?>><a href="<?=site_url()?>">Beranda</a></li>
					<li <?=current_url()==site_url('site/home/catalog')?'class="colorlib-active"':''?>><a href="<?=site_url('site/home/gallery')?>">Katalog</a></li>
					<li <?=current_url()==site_url('site/home/post/7')?'class="colorlib-active"':''?>><a href="<?=site_url('site/home/post/7')?>">Artikel</a></li>
					<li <?=current_url()==site_url('site/home/contact')?'class="colorlib-active"':''?>><a href="<?=site_url('site/home/contact')?>">Kontak</a></li>
				</ul>
			</nav>

			<div class="colorlib-footer">
				<p>
					<small>
						&copy; Copyright <script>document.write(new Date().getFullYear());</script>
						<span>Developed by <a href="https://www.linkedin.com/in/yoelrolas/" target="_blank">Partopi tao</a></span>
					</small>
				</p>
				<ul>
					<li><a href="https://wa.me/628116545592"><i class="icon-whatsapp"></i></a></li>
					<li><a href="https://wa.me/6281377272622"><i class="icon-whatsapp"></i></a></li>
					<li><a href="https://www.instagram.com/medan.wallpaper/"><i class="icon-instagram"></i></a></li>
					<li><a href="https://www.instagram.com/cahayainteriormedan/"><i class="icon-instagram"></i></a></li>
				</ul>
			</div>

		</aside>

		<div id="colorlib-main">
			<?=$content?>
		</div>
	</div>

	<!-- jQuery -->
	<script src="<?=base_url()?>assets/themes/balay/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?=base_url()?>assets/themes/balay/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?=base_url()?>assets/themes/balay/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?=base_url()?>assets/themes/balay/js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="<?=base_url()?>assets/themes/balay/js/jquery.flexslider-min.js"></script>
	<!-- Sticky Kit -->
	<script src="<?=base_url()?>assets/themes/balay/js/sticky-kit.min.js"></script>
	<!-- Owl carousel -->
	<script src="<?=base_url()?>assets/themes/balay/js/owl.carousel.min.js"></script>
	<!-- Counters -->
	<script src="<?=base_url()?>assets/themes/balay/js/jquery.countTo.js"></script>


	<!-- MAIN JS -->
	<script src="<?=base_url()?>assets/themes/balay/js/main.js"></script>

	</body>
</html>
